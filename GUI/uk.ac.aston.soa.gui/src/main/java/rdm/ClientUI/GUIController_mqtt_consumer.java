package rdm.ClientUI;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.eclipse.paho.client.mqttv3.MqttException;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class GUIController_mqtt_consumer {

	private final class PeriodicQueryTask extends TimerTask {
		@Override
		public void run() {
			try {
				cont++;
				List<String> observations = rdmClient.getList();  
				double MEC= mqttsubscriber.beliefMEC;
				double MR= mqttsubscriber.beliefMR; 
				double MP= mqttsubscriber.beliefMP;
				aveMEC = mqttsubscriber.aveMEC;
				aveMR = mqttsubscriber.aveMR;
				aveMP = mqttsubscriber.aveMP;
				topology = mqttsubscriber.topology;
				tsID=String.valueOf(mqttsubscriber.tsID);
				LTT=mqttsubscriber.getLongTerm();
				Platform.runLater(() -> {
					// Update the UI here
					serieMC.getData().add(new XYChart.Data<String, Number>(tsID,MEC));
					serieMR.getData().add(new XYChart.Data<String, Number>(tsID,MR)); 
					serieMP.getData().add(new XYChart.Data<String, Number>(tsID,MP));
					thresholdMEC.getData().add(new XYChart.Data<String, Number>(tsID,threshMEC));
					thresholdMR.getData().add(new XYChart.Data<String, Number>(tsID,threshMR));
					thresholdMP.getData().add(new XYChart.Data<String, Number>(tsID,threshMP));
					lblObservationMC.setText("Satisficement MEC: "+ MEC);
					lblObservationMR.setText("Satisficement MR: " + MR);
					lblObservationMP.setText("Satisficement MP: "+ MP);
					lblCurrentTopology.setText(""+ topology);	
					lblWeightMC.setText("Weight prioritization MEC: "+ observations.get(5));
					lblWeightMR.setText("Weight prioritization MR: "+ observations.get(6));
					lblWeightMP.setText("Weight prioritization MP: "+ observations.get(7));
					tableView.setItems(LTT);
					showAverages();
				});
				// Query again in 1s
				queryTimer.schedule(new PeriodicQueryTask(),1000);
				
			} catch (TException e) {
				e.printStackTrace();
			}
		}
	}

	@FXML private Label lblObservationMC;
	@FXML private Label lblObservationMR;
	@FXML private Label lblObservationMP;

	@FXML private Label lblWeightMC;
	@FXML private Label lblWeightMR;
	@FXML private Label lblWeightMP;
	
	@FXML private Label lblCurrentTopology;
	String topology;
	@FXML private Label lblAveMEC;
	@FXML private Label lblAveMR;
	@FXML private Label lblAveMP;
	@FXML private Label lblAveMECts;
	@FXML private Label lblAveMRts;
	@FXML private Label lblAveMPts;
	double aveMEC= 0;
	double aveMR = 0;
	double aveMP = 0;
	double threshMEC = 0.8;
	double threshMR = 0.9;
	double threshMP = 0.75;
	
	String tsID;
	
	@FXML CategoryAxis xAxis;
    @FXML NumberAxis yAxisMEC,yAxisMR,yAxisMP;
    
    @FXML XYChart.Series<String,Number> serieMC= new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMEC = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> serieMR = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMR = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> serieMP = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMP = new Series<String, Number>();
    @FXML private LineChart<String,Number> chartMC;
    @FXML private LineChart<String,Number> chartMR;
    @FXML private LineChart<String,Number> chartMP;
    
    //Data for Table view
    @FXML private TableView<LongTermEffect> tableView;
    @FXML private TableColumn<LongTermEffect, Long> timeStampColumn;
    @FXML private TableColumn<LongTermEffect, Integer> IDColumn;
    @FXML private TableColumn<LongTermEffect, String > actionColumn;
    @FXML private TableColumn<LongTermEffect, Double > satisfismentColumn;
    @FXML private final SimpleStringProperty action = new SimpleStringProperty();
    
	// Client to C++ program
	private Something.Client rdmClient;

	// Mqtt client
	MqttSubscriber mqttsubscriber;
	
	//TScounter
	int cont;
	//Timer for tasks
	private final Timer queryTimer = new Timer();
	ObservableList<LongTermEffect> LTT;
	
	@SuppressWarnings("unchecked")
	@FXML
	protected void initialize() throws IOException, TException, InterruptedException, URISyntaxException, MqttException {
		try {
			// Connect to Broker
			System.out.println("Connect to the broker");
			String topic = "RDM_logs1"; 
		    String broker       = "tcp://localhost:1883";
		    String clientId     = "UI";
			mqttsubscriber = new  MqttSubscriber(broker,clientId,topic);

			// Connect to RDM
			final int port = 9090;
			TSocket transport = new TSocket("localhost", port);
			transport.open();
			final TCompactProtocol protocol = new TCompactProtocol(transport);
			System.out.println("Connecting client on port " + String.valueOf(port));
			rdmClient = new Something.Client(protocol);

			//Counter
			cont=0;
			//// Initializing charts
			//Axis
			yAxisMEC.setAutoRanging(false);
		    yAxisMEC.setLowerBound(0.5);
		    yAxisMEC.setUpperBound(1);
		    yAxisMEC.setTickUnit(0.1);
			yAxisMR.setAutoRanging(false);
		    yAxisMR.setLowerBound(0.5);
		    yAxisMR.setUpperBound(1);
		    yAxisMR.setTickUnit(0.1);
			yAxisMP.setAutoRanging(false);
		    yAxisMP.setLowerBound(0.5);
		    yAxisMP.setUpperBound(1);
		    yAxisMP.setTickUnit(0.1);
		    //Series 
			serieMC.setName("MEC");
			serieMR.setName("MR");
			serieMP.setName("MP");
			thresholdMEC.setName("Threshold MEC");
			thresholdMP.setName("Threshold MP");
			thresholdMR.setName("Threshold MR");
			//charts
			chartMC.getData().addAll(serieMC,thresholdMEC);
			chartMC.setCreateSymbols(false);
			chartMR.getData().addAll(serieMR,thresholdMR);
			chartMR.setCreateSymbols(false);
			chartMP.getData().addAll(serieMP,thresholdMP);
			chartMP.setCreateSymbols(false);
			//Initializing averages labels
			lblAveMEC.setText(""+aveMEC);
			lblAveMR.setText(""+aveMR);
			lblAveMP.setText(""+aveMP);
			//Initilizing averages TS
			lblAveMECts.setText("MEC Average at ts: "+0);
			lblAveMRts.setText("MR Average at ts: "+0);
			lblAveMPts.setText("MP Average at ts: "+0);
			//Initializing tableView
			timeStampColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Long>("ts"));
			IDColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Integer>("id"));
			actionColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,String>("selected_action"));
			satisfismentColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Double>("current_belief_mec_true_s"));
			
			// Start periodic queries of RDM weights
			queryTimer.schedule(new PeriodicQueryTask(), 1000);
		} catch (TTransportException e) {
			e.printStackTrace();
		} 
	}

	@FXML public void onIncreaseMC() throws TException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Increasing priority of MEC");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.8, 0.1, 0.1);
			showAverages();
		} 
	}
	
	@FXML public void onDecreaseMC() throws TException{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Decreasing priority of MEC");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.1, 0.45, 0.45);
			showAverages();
		}

		
	}

	@FXML public void onIncreaseMR() throws TException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Increasing priority of MR");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.1, 0.85, 0.05);
			showAverages();
		}
		
	}
	
	@FXML public void onDecreaseMR() throws TException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Decreasing priority of MR");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.5, 0.2, 0.3);
			showAverages();
		}
		
	}

	@FXML public void onIncreaseMP() throws TException {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Increasing priority of MP");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.1, 0.2, 0.7);
			showAverages();
		}
	}
	
	@FXML public void onDecreaseMP() throws TException {

	    /*
	     
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Decreasing priority of MP");
		alert.setHeaderText("Dear user,\n \n"
				+ " The averages for the non-functional requirements until the time-slice "+tsID+" have been: \n"+
				" AveMEC: "+aveMEC+" and its threshold: "+threshMEC +
				"\n AveMR: "+aveMR+" and its threshold: "+threshMR +
				"\n AveMP: "+aveMP+" and its threshold: "+threshMP +"\n"
				);
		alert.setContentText("Are you sure to continue with this action?");
		java.util.Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			rdmClient.modifyPOMWeights(0.4, 0.5, 0.1);
			showAverages();
		}*/
	
		showAverages();
	}

	public void showAverages() {
		Platform.runLater(() ->{
			lblAveMEC.setText(""+aveMEC);
			lblAveMR.setText(""+aveMR);
			lblAveMP.setText(""+aveMP);
			lblAveMECts.setText("MEC Average at ts: "+tsID);
			lblAveMRts.setText("MR Average at ts: "+tsID);
			lblAveMPts.setText("MP Average at ts: "+tsID);
				});
	}
}


