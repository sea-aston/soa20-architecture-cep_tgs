package rdm.ClientUI;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.thrift.TException;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MqttSubscriber {
  
	public  String logTopic;
	public static String eventsTopic;
    public String broker;
    public String clientId;
    static int qos = 0;  
    static MemoryPersistence persistence = new MemoryPersistence();
    static MqttMessage message;
    static MqttClient mqttClient;
	public double beliefMEC;
	public double beliefMR;
	public double beliefMP;
	public double aveMEC;
	public double aveMR;
	public double aveMP;
	public String topology;
	public int tsID;
	public Long timestamp;
	JsonNode jsonlog;
	public int counter;
	public double sumMEC;
	public double sumMR;
	public double sumMP;
	ObservableList<LongTermEffect> longTerm = FXCollections.observableArrayList();
	public MqttSubscriber(String broker, String clientId, String topic) 
			throws URISyntaxException, IOException, TException, InterruptedException, MqttException {
		this.broker = broker;
		this.clientId = clientId;
		this.logTopic = topic;
		run();
	}
	
	public void run() throws IOException, TException, InterruptedException, MqttException {
		System.out.println("MQTT CLIENT");
		counter=0;
		sumMEC=0;
		sumMR=0;
		sumMP=0;
		eventsTopic="output"; 
		//Connect to the broker 
		 mqttClient = new MqttClient(broker, clientId, persistence);
	        MqttConnectOptions connOpts = new MqttConnectOptions();
	        connOpts.setCleanSession(false); 
        //Set Callbacks
        mqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) { 
            	//Called when the client lost the connection to the broker 
            	System.out.println("Connection to localhost broker lost! " + cause.getMessage());
            }
            @Override
            public void messageArrived(String topic1, MqttMessage message1) throws Exception {
            	String content = new String(message1.getPayload());
        	    ObjectMapper mapper = new ObjectMapper();
        	    jsonlog = mapper.readTree(content);
            	if(topic1.equals(logTopic)) {
            		//System.out.println(topic1 + ": " + content);
            		counter++;
            	    timestamp = jsonlog.get("ts").asLong();
            	    tsID=jsonlog.get("id").asInt();
            	    topology=jsonlog.get("selected_action").toString();
            	    beliefMEC = jsonlog.get("current_belief_mec_true_s").asDouble();
            	    beliefMR = jsonlog.get("current_belief_mr_true_s").asDouble();
            	    beliefMP = jsonlog.get("current_belief_mp_true_s").asDouble();
            	    sumMEC+=beliefMEC;
            	    sumMR+=beliefMR;
            	    sumMP+=beliefMP;
            	    aveMEC=sumMEC/counter;
            	    aveMR=sumMR/counter;
            	    aveMP=sumMP/counter;
            	}else if(topic1.equals(eventsTopic)) {
            		System.out.println(topic1 + ": " + content);
            		longTerm.add(new LongTermEffect(jsonlog.get("ts").asLong(),
            				jsonlog.get("selected_action").toString(),
            				jsonlog.get("id").asInt(),
            				jsonlog.get("current_belief_mec_true_s").asDouble(),
            				0.9,
            				"MC"));
            	}
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
            	//Called when a outgoing publish is complete 
            }
        });
        //Conecting and subscribing to MQTT
        System.out.println("Connecting to broker: "+broker);
        mqttClient.connect(connOpts); 
        System.out.println("Connected");
        System.out.println("Subscribing client to topic: " + logTopic);
        String [] topicFilters = {logTopic, eventsTopic};
        mqttClient.subscribe(topicFilters);
        
        System.out.println("Subscribed");
		
	}
	public ObservableList<LongTermEffect>  getLongTerm(){
		return longTerm;
		
	}

}
