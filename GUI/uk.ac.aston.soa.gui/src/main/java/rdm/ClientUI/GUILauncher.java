package rdm.ClientUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUILauncher extends Application {

	@Override 
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setController(new GUIController_SOA20_mqtt());
		loader.setLocation(getClass().getResource("GUI-SOA-2.0_v2.fxml"));
		Parent parent = loader.load(); 

		Scene scene = new Scene(parent, 800, 600);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Runtime Monitoring SEA-UCA");
		primaryStage.show();
	}
 
	public static void main(String[] args) {
		launch(args);
	}
}
