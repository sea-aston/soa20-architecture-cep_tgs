package rdm.ClientUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSlider;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UserInterface {

	private JFrame frame;
	public String name;
	List<String> array;
	private JTextField TextConsole;
	ThriftClient client;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInterface window = new UserInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("User Interface");
		frame.setBounds(100, 100, 800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JSlider slider = new JSlider();
		frame.getContentPane().add(slider, BorderLayout.EAST);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ThriftClient t1 = new ThriftClient();
				t1.start();
				}
		});
		frame.getContentPane().add(btnConnect, BorderLayout.SOUTH);
		
		TextConsole = new JTextField();
		frame.getContentPane().add(TextConsole, BorderLayout.CENTER);
		TextConsole.setColumns(10);
		
	}

}
