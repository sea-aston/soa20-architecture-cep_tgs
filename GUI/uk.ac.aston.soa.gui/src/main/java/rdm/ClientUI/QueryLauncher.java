
package rdm.ClientUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.apache.thrift.TException;
import org.hawk.service.api.FailedQuery;
import org.hawk.service.api.Hawk;
import org.hawk.service.api.Hawk.Client;
import org.hawk.service.api.HawkQueryOptions;
import org.hawk.service.api.QueryReport;
import org.hawk.service.api.QueryResult;
import org.hawk.service.api.utils.APIUtils;

public class QueryLauncher {

	private final Client hawk;
	private final String hawkInstance;
	private final File eolPath;
	public double beliefMEC;
	public double beliefMR;
	public double beliefMP;
	public double aveMEC;
	public double aveMR;
	public double aveMP;
	public String topology,tsID;
	public QueryReport result;
	 
 
	public QueryLauncher(String hawkURL, String hawkInstance, File eolPath)
			throws URISyntaxException, IOException, TException, InterruptedException {
		this.hawk = APIUtils.connectTo(Hawk.Client.class, hawkURL);
		this.hawkInstance = hawkInstance;
		this.eolPath = eolPath; 
		run(); 
	}

	public void run() throws IOException, TException, InterruptedException {
		// 1. load eolQuery from eolFile...
		final long startMillis1 = System.currentTimeMillis();
		String eolQuery;
		try (FileReader fR = new FileReader(eolPath); BufferedReader br = new BufferedReader(fR)) {
			final StringBuilder sb = new StringBuilder();
			for (String line; (line = br.readLine()) != null; ) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			eolQuery = sb.toString().trim();
			
		}
		if (eolQuery.isEmpty()) {
			System.err.println("No query to run");
			return;
		}

		// 2. Tell Hawk to synchronize
		hawk.syncInstance(hawkInstance, true);
		
		// 3. Run the query (retry with short delay if it fails, but log the problem)
		int tries = 0;
		result = null;
		for (; tries < 5 && result == null; tries++) {
			try {
				result = hawk.timedQuery(hawkInstance, eolQuery,
						"org.eclipse.hawk.timeaware.queries.TimeAwareEOLQueryEngine", new HawkQueryOptions());
				
				
			} catch (FailedQuery e) {
				e.printStackTrace();
				synchronized(this) {
					Thread.sleep(500);
				}
			} catch (TException e) {
				throw e;
			}
		}
		final long finalMillis = System.currentTimeMillis() - startMillis1;
		System.out.println(
				String.format("Produced result for %s took %d ms",eolPath,finalMillis));
		/*
		beliefMEC=result.getResult().getVList().get(0).getVList().get(2).getVDouble();
		beliefMR=result.getResult().getVList().get(1).getVList().get(2).getVDouble();
		beliefMP=result.getResult().getVList().get(2).getVList().get(2).getVDouble();
		aveMEC= result.getResult().getVList().get(0).getVList().get(3).getVDouble();
		aveMR= result.getResult().getVList().get(0).getVList().get(4).getVDouble();
		aveMP= result.getResult().getVList().get(0).getVList().get(5).getVDouble();
		topology = result.getResult().getVList().get(0).getVList().get(6).getVString();
		tsID = result.getResult().getVList().get(0).getVList().get(7).getVString();
		//System.out.println(result);
		System.out.println("Ts: "+tsID+" MEC: "+beliefMEC+" MR: "+beliefMR+" MP: "+beliefMP +
				" AvMEC: "+ aveMEC + " AvMR: "+ aveMR+" AvMP: "+aveMP+" Topology: "+topology);
		*/
	}
	/*
	public static void main(String[] args) throws IOException {
		if (args.length != 3) {
			System.err.println("Usage: hawkURL hawkInstance eolPath"); 
			System.exit(1); 
		}

		final String hawkURL = args[0];
		final String hawkInstance = args[1];
		final File eolPath = new File(args[2]);

		try {
			for (int i=0; i<1000;i++) {
				
			new QueryLauncher(hawkURL, hawkInstance, eolPath).run();
			TimeUnit.SECONDS.sleep(1);
			}
		} catch (URISyntaxException | TException | InterruptedException e) {
			e.printStackTrace();
			System.exit(2);
		}
	}*/

}
