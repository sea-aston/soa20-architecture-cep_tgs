package rdm.ClientUI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.eclipse.paho.client.mqttv3.MqttException;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class GUIController_SOA20_mqtt {
	String selectedQuery;
	private final class PeriodicQueryTask extends TimerTask {
		@Override
		public void run() {
		try {
			cont++;
			double MEC= mqttsubscriber.beliefMEC;
			double MR= mqttsubscriber.beliefMR; 
			double MP= mqttsubscriber.beliefMP;
			aveMEC = mqttsubscriber.aveMEC;
			aveMR = mqttsubscriber.aveMR;
			aveMP = mqttsubscriber.aveMP;
			topology = mqttsubscriber.topology;
			tsID=String.valueOf(mqttsubscriber.tsID);
			LTT=mqttsubscriber.getLongTerm();
			Platform.runLater(() -> {
				// Update the UI here
				serieMC.getData().add(new XYChart.Data<String, Number>(tsID,MEC));
				serieMR.getData().add(new XYChart.Data<String, Number>(tsID,MR)); 
				serieMP.getData().add(new XYChart.Data<String, Number>(tsID,MP));
				thresholdMEC.getData().add(new XYChart.Data<String, Number>(tsID,threshMEC));
				thresholdMR.getData().add(new XYChart.Data<String, Number>(tsID,threshMR));
				thresholdMP.getData().add(new XYChart.Data<String, Number>(tsID,threshMP));
				lblCurrentTopology.setText(""+ topology);
				if (!LTT.isEmpty()) {
				tableView.setItems(LTT);}
			});
			// Query again in 1s
			queryTimer.schedule(new PeriodicQueryTask(),1000);
			}catch(Exception e) {
			e.printStackTrace();
			
			}
		}
	}

	
	@FXML private Label lblCurrentTopology;
	String topology;
	double aveMEC= 0;
	double aveMR = 0;
	double aveMP = 0;
	double threshMEC = 0.8;
	double threshMR = 0.9;
	double threshMP = 0.75;
	
	String tsID;
	
	@FXML CategoryAxis xAxis;
    @FXML NumberAxis yAxisMEC,yAxisMR,yAxisMP;
    
    @FXML XYChart.Series<String,Number> serieMC= new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMEC = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> serieMR = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMR = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> serieMP = new Series<String, Number>();
    @FXML XYChart.Series<String,Number> thresholdMP = new Series<String, Number>();
    @FXML private LineChart<String,Number> chartMC;
    @FXML private LineChart<String,Number> chartMR;
    @FXML private LineChart<String,Number> chartMP;
    
    //Data for Table view results
    @FXML private TableView<LongTermEffect> tableView;
    @FXML private TableColumn<LongTermEffect, Long> timeStampColumn;
    @FXML private TableColumn<LongTermEffect, Integer> IDColumn;
    @FXML private TableColumn<LongTermEffect, String > actionColumn;
    @FXML private TableColumn<LongTermEffect, Double > satisfismentColumn;
    @FXML private TableColumn<LongTermEffect, Double > SLAColumn;
    @FXML private TableColumn<LongTermEffect, String > NFRColumn;
    @FXML private final SimpleStringProperty action = new SimpleStringProperty();
    @FXML private final SimpleStringProperty nfr = new SimpleStringProperty();
    
    //Data for choice box preloaded queries
    ObservableList queryList=FXCollections.observableArrayList();
    @FXML private ChoiceBox<String> queriesBox;
    //TextFile for queries
    @FXML private TextArea queryArea;
    @FXML private TextArea queryResultArea;
    
	// Client to C++ program
	private Something.Client rdmClient;

	// Mqtt client
	MqttSubscriber mqttsubscriber;
	
	//TScounter
	int cont;
	//Timer for tasks
	private final Timer queryTimer = new Timer();
	ObservableList<LongTermEffect> LTT;
	
	@SuppressWarnings("unchecked")
	@FXML
	protected void initialize() throws IOException, TException, InterruptedException, URISyntaxException, MqttException {
		try {
			// Connect to Broker
			System.out.println("Connect to the broker");
			String topic = "RDM_logs1"; 
		    String broker       = "tcp://localhost:1883";
		    String clientId     = "UI";
			mqttsubscriber = new  MqttSubscriber(broker,clientId,topic);

			// Connect to RDM
			//final int port = 9090;
			//TSocket transport = new TSocket("localhost", port);
			//transport.open();
			//final TCompactProtocol protocol = new TCompactProtocol(transport);
			//System.out.println("Connecting client on port " + String.valueOf(port));
			//rdmClient = new Something.Client(protocol);

			//Counter
			cont=0;
			//// Initializing charts
			//Axis
			yAxisMEC.setAutoRanging(false);
		    yAxisMEC.setLowerBound(0.5);
		    yAxisMEC.setUpperBound(1);
		    yAxisMEC.setTickUnit(0.1);
			yAxisMR.setAutoRanging(false);
		    yAxisMR.setLowerBound(0.5);
		    yAxisMR.setUpperBound(1);
		    yAxisMR.setTickUnit(0.1);
			yAxisMP.setAutoRanging(false);
		    yAxisMP.setLowerBound(0.5);
		    yAxisMP.setUpperBound(1);
		    yAxisMP.setTickUnit(0.1);
		    //Series 
			serieMC.setName("MEC");
			serieMR.setName("MR");
			serieMP.setName("MP");
			thresholdMEC.setName("Threshold MEC");
			thresholdMP.setName("Threshold MP");
			thresholdMR.setName("Threshold MR");
			//charts
			chartMC.getData().addAll(serieMC,thresholdMEC);
			chartMC.setCreateSymbols(false);
			chartMR.getData().addAll(serieMR,thresholdMR);
			chartMR.setCreateSymbols(false);
			chartMP.getData().addAll(serieMP,thresholdMP);
			chartMP.setCreateSymbols(false);

			//Initializing tableView
			timeStampColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Long>("ts"));
			IDColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Integer>("id"));
			actionColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,String>("selected_action"));
			satisfismentColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Double>("current_belief_mec_true_s"));
			NFRColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,String>("nfr"));
			SLAColumn.setCellValueFactory(new PropertyValueFactory<LongTermEffect,Double>("sla"));
			//Initializing checbox list (preloaded queries)
			queryList.removeAll(queryList);
			String a="Long Term Effect of Maximization of Reliability MR";
			String b="Long Term Effect of Minimization of Energy Consumption MC"; 
			String c="Long Term Effect of Maximization of Performance MP";
			queryList.addAll(a,b,c);
			queriesBox.getItems().addAll(queryList);
			//Query results field text
			queryResultArea.setWrapText(true);
			
			// Start periodic queries of RDM weights
			queryTimer.schedule(new PeriodicQueryTask(), 1000);
		} catch (TTransportException e) {
			e.printStackTrace();
		} 
	}
	 
	//LoadQuery Button
	@FXML private void displayQuery(ActionEvent event) throws FileNotFoundException, IOException {
		selectedQuery=queriesBox.getValue();
		System.out.println("button pressed selection>"+selectedQuery);
		if(!selectedQuery.isEmpty()) {
			if(selectedQuery.contains("MR")) {
				String queryText=loadQuery("/home/CAMPUS/parra-uj/git/rdm_and_more/ClientUI/Resources/LongTermEffectMR-NoPrimitives.eol");
				queryArea.setText(queryText);
			}if(selectedQuery.contains("MP")){
				String queryText=loadQuery("/home/CAMPUS/parra-uj/git/rdm_and_more/ClientUI/Resources/LongTermEffectMP-NoPrimitives.eol");
				queryArea.setText(queryText);
			}
		}
	}
	private String loadQuery(String eolPath) throws FileNotFoundException, IOException {
		String eolQuery;
		final File eolPath1 = new File(eolPath);
		try (FileReader fR = new FileReader(eolPath1); BufferedReader br = new BufferedReader(fR)) {
			final StringBuilder sb = new StringBuilder();
			for (String line; (line = br.readLine()) != null; ) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			eolQuery = sb.toString().trim();
			return eolQuery;
		}
	}
	@FXML private void runQuery(ActionEvent event) throws URISyntaxException, IOException, TException, InterruptedException {
		QueryLauncher queryLauncher;
		String hawkURL, hawkInstance;
		String eolPath ="";
		if(!selectedQuery.isEmpty()) {
			if(selectedQuery.contains("MR")) {
				 eolPath = "/home/CAMPUS/parra-uj/git/rdm_and_more/ClientUI/Resources/LongTermEffectMC-NoPrimitives.eol";
			}if(selectedQuery.contains("MP")){
				 eolPath = "/home/CAMPUS/parra-uj/git/rdm_and_more/ClientUI/Resources/LongTermEffectMP-NoPrimitives.eol";
			}
		}
		
		// Connect to Hawk
		hawkURL = "http://localhost:8080/thrift/hawk/tuple";
		hawkInstance = "RDMInstance";
		final File eolPath1 = new File(eolPath);
		queryLauncher = new QueryLauncher (hawkURL, hawkInstance, eolPath1);
		if(!(queryLauncher.result.toString().isEmpty())) {
			queryResultArea.setText(queryLauncher.result.toString());
			}else {System.out.println("Empty query result");}
		
	}

}


