package rdm.ClientUI;

import javafx.beans.property.SimpleStringProperty;

public class LongTermEffect {
	public Long ts;
	public SimpleStringProperty selected_action;
	public int id;
	public Double current_belief_mec_true_s;
	public Double sla;
	public SimpleStringProperty nfr;
	
	public LongTermEffect(Long ts, String selected_action, int id, Double current_belief_mec_true_s,
			Double sla, String nfr) {
		super();
		this.ts = ts;
		this.selected_action = new SimpleStringProperty(selected_action);
		this.id = id;
		this.current_belief_mec_true_s = current_belief_mec_true_s;
		this.sla = sla;
		this.nfr = new SimpleStringProperty(nfr);
	}
	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	public Double getSla() {
		return sla;
	}
	public void setSla(Double sla) {
		this.sla = sla;
	}
	public String getNfr() {
		return nfr.get();
	}
	public void setNfr(SimpleStringProperty nfr) {
		this.nfr = nfr;
	}
	public String getSelected_action() {
		return selected_action.get();
	}

	public void setSelected_action(SimpleStringProperty selected_action) {
		this.selected_action = selected_action;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getCurrent_belief_mec_true_s() {
		return current_belief_mec_true_s;
	}

	public void setCurrent_belief_mec_true_s(Double current_belief_mec_true_s) {
		this.current_belief_mec_true_s = current_belief_mec_true_s;
	}
	
}
