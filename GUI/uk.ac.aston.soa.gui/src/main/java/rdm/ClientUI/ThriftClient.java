package rdm.ClientUI;
import java.util.List;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.hawk.service.api.Hawk;
import org.hawk.service.api.utils.APIUtils;
public class ThriftClient extends Thread {
	static List<String> array;
	static List<String> array1;
	static boolean bandera=true;
	static TTransport transport;
	static TProtocol protocol;
	static Something.Client client;
	
	public static void main (String[] args) throws TException, InterruptedException {

		
		
		connect();
		readValues();
		disconnect();
	}

	public static void connect() {
		try {
			
			Hawk.Client hawk = APIUtils.connectTo(Hawk.Client.class, "http://url/to/hawk/tuple");
			/*QueryReport result;
			HawkQueryOptions hawkQueryoptions;
			client.timedQuery(result, sCommand, query, language, hawkQueryoptions);
			*/
			
			int port = 9090;
			transport = new TSocket ("localhost",port);
			transport.open();
			protocol = new TCompactProtocol(transport);
			System.out.println("Connecting client on port "+ String.valueOf(port));
			client = new Something.Client(protocol); 
		}catch (Exception x) {
			x.printStackTrace();
		}

	}
	public static void readValues() throws TException, InterruptedException {
		for (int i=0; i<100;i++) {
			array = client.getList();
			setArray(array);
			array1 = client.getReward();
			for (String item: getArray()) {
				System.out.println(item);				 
			}				
			if (i>30) {
				System.out.println("Modifying POM Weights");
				client.modifyPOMWeights(0.1, 0.6, 0.3);
			}
			System.out.println("Rewards Matrix");
			for (int j=0;j<=array1.size()-1;j++) {
				
				if(j<8) {
					System.out.print(array1.get(j)+ " ");
				}else {
					if(bandera) {
						System.out.println(" ");
						bandera = false;
					}
					System.out.print(array1.get(j)+ " ");
				}
			}
			Thread.sleep(1000);
			System.out.println(" ");
			bandera = true;
		}
	}
	private static void disconnect() {
		transport.close();
		System.out.println("End"); 
	}
	public static void setArray(List<String> array1) {
		array = array1;
	}
	public static List<String> getArray(){
		return array;
		
	}

	public void run() {
		System.out.println("Thread");
		connect();
		try {
			readValues();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
	}
}