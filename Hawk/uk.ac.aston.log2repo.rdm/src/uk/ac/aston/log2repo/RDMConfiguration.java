package uk.ac.aston.log2repo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RDMConfiguration {

	@JsonProperty("criteria_poor_high")
	double[] nfrThresholds;

	public double[] getNfrThresholds() {
		return nfrThresholds;
	}

	public void setNfrThresholds(double[] nfrThresholds) {
		this.nfrThresholds = nfrThresholds;
	}

}
