package uk.ac.aston.log2repo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnCommit;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnScheduleForAddition;
import org.tmatesoft.svn.core.wc2.SvnTarget;
import org.tmatesoft.svn.core.wc2.SvnUpdate;
import org.tmatesoft.svn.core.wc2.admin.SvnRepositoryCreate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import uk.ac.aston.stormlog.Log;

/**
 * Converts a STORM all-slices log into an SVN repository.
 */
public class AllSlicesLogConverter {

	private final File checkoutDir;
	private final Map<String, TimeSliceEntry> rawLog;

	private final SvnOperationFactory svnOperationFactory;
	private final SVNClientManager svnClientManager;
	private final SVNURL svnRepoURL;
	private final RDMConfiguration rdmConfig;

	public AllSlicesLogConverter(File rdmFile, File jsonLog, File svnRepo, File checkoutDir) throws JsonParseException, JsonMappingException, IOException, SVNException {
		final ObjectMapper om = new ObjectMapper();

		this.rdmConfig = om.readValue(rdmFile, RDMConfiguration.class);
		this.rawLog = om.readValue(jsonLog, new TypeReference<Map<String, TimeSliceEntry>>() {});
		this.checkoutDir = checkoutDir;

		svnOperationFactory = new SvnOperationFactory();
		svnClientManager = SVNClientManager.newInstance();

		SvnRepositoryCreate svnCreate = svnOperationFactory.createRepositoryCreate();
		svnCreate.setRepositoryRoot(svnRepo.getCanonicalFile());
		svnRepoURL = svnCreate.run();

		SvnCheckout svnCheckout = svnOperationFactory.createCheckout();
		svnCheckout.setSource(SvnTarget.fromURL(svnRepoURL));
		svnCheckout.setSingleTarget(SvnTarget.fromFile(checkoutDir));
		svnCheckout.run();

		svnClientManager.createRepository(SVNURL.fromFile(checkoutDir), true);
	}

	public void convert() throws NumberFormatException, IOException, SVNException {
		final long start = System.nanoTime();
		boolean first = true;

		for (Entry<String, TimeSliceEntry> entry : rawLog.entrySet()) {
			final long startEntry = System.nanoTime();
			File modelFile = convert(entry.getKey(), entry.getValue());

			if (first) {
				first = false;
				SvnScheduleForAddition svnAdd = svnOperationFactory.createScheduleForAddition();
				svnAdd.addTarget(SvnTarget.fromFile(modelFile));
				svnAdd.run();
			}

			final SvnTarget checkoutTarget = SvnTarget.fromFile(checkoutDir);
			SvnCommit svnCommit = svnOperationFactory.createCommit();
			svnCommit.setSingleTarget(checkoutTarget);
			svnCommit.setCommitMessage("Committing slice " + entry.getKey());
			svnCommit.run();

			final SvnUpdate svnUp = svnOperationFactory.createUpdate();
			svnUp.setSingleTarget(checkoutTarget);
			svnUp.run();
			final long endEntry = System.nanoTime();
			final long startMillis = (endEntry - start) / 1_000_000;
			final long startEntryMillis = (endEntry - startEntry) / 1_000_000;

			System.out.println(String.format(   
				"Converted slice %s (took %d ms) - %d ms since start",
				entry.getKey(), startEntryMillis, startMillis));
		}
	}

	private File convert(String timesliceID, TimeSliceEntry sliceInfo) throws IOException {
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());

		final File modelFile = new File(checkoutDir, "log.xmi");
		final Resource r = rs.createResource(URI.createFileURI(modelFile.getAbsolutePath()));
		final Log log = new TimeSliceConverter().convert(rdmConfig, timesliceID, sliceInfo);

		r.getContents().add(log);
		r.save(null);
		r.unload();

		return modelFile;
	}

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Usage: path/to/rdm.json path/to/log.json path/to/newrepo");
			System.exit(1);
		}
		System.out.println(args[0]);
		System.out.println(args[1]);
		System.out.println(args[2]);
		File fCheckoutDir = null;
		try {
			fCheckoutDir = Files.createTempDirectory("stormco").toFile();

			// Delete and recreate the repository
			final File newRepo = new File(args[2]);
			if (newRepo.exists()) {
				FileUtils.deleteDirectory(newRepo);
			} else {
				newRepo.mkdirs();
			}

			new AllSlicesLogConverter(new File(args[0]), new File(args[1]), newRepo, fCheckoutDir).convert();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fCheckoutDir != null) {
				try {
					FileUtils.deleteDirectory(fCheckoutDir);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
