@GenModel(complianceLevel="8.0")
package uk.ac.aston.stormlog

class Log {
	String timesliceID
	contains Decision[] decisions
	contains Action[] actions
	contains NFR[] requirements
	contains Measure[] measures
	contains Observation[] observations

	op Measure getMeasure(String measureName) {
		for (Measure m : measures) {
			if (m.name == measureName) return m
		}
		return null
	}
	op Action getAction(String actionName) {
		for (Action a : actions) {
			if (a.name == actionName) return a
		}
		return null
	}
}

class Decision {
	String name
	contains RewardTable rewardTable
	contains NFRBelief[] nfrBeliefsPre
	contains NFRBelief[] nfrBeliefsPost
	contains ActionBelief[] actionBeliefs
	refers Observation observation
	refers Action actionTaken
	refers RewardTableRow actualReward
}

class Observation {
	String description
	double probability
	contains Measurement[] measurements

	op Measurement getFirstMeasurement(String measureName) {
		for (Measurement m : measurements) {
			if (m.measure.name == measureName) return m
		}
		return null
	}

	op Measurement getFirstMeasurement(Measure measure) {
		for (Measurement m : measurements) {
			if (m.measure == measure) return m
		}
		return null
	}
}

abstract class Measurement {
	refers Measure measure
}

class Measure {
	String name
	// ordered list of thresholds for the metric
	contains Threshold[] thresholds
}

class Threshold {
	String name
	double value
}

class NFRBelief {
	refers NFR nfr
	double estimatedProbability
	boolean satisfied
}

class ActionBelief {
	refers Action action
	double estimatedValue
}

class Action {
	String name
}

class NFR {
	String name
	refers Measure[] measure //////
}

class RewardTable {
	contains RewardTableRow[] rows
	contains RewardTableThreshold[] thresholds //////////////////
}

class NFRSatisfaction {
	refers NFR nfr
	boolean satisfied
}

class RewardTableRow {
	contains NFRSatisfaction[] satisfactions
	refers Action action
	double value
}

class RewardTableThreshold {
	double value
	refers NFR nfr
}

class DoubleMeasurement extends Measurement {
	double value
}

class IntegerMeasurement extends Measurement {
	int value
}

class StringMeasurement extends Measurement {
	String value
}

class RangeMeasurement extends Measurement {  
	int position
}

class DoubleListMeasurement extends Measurement {
	double[] value	
}

class IntegerListMeasurement extends Measurement {
	int[] value
}