/**
 */
package uk.ac.aston.stormlog.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import uk.ac.aston.stormlog.NFR;
import uk.ac.aston.stormlog.NFRBelief;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NFR Belief</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.NFRBeliefImpl#getNfr <em>Nfr</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.NFRBeliefImpl#getEstimatedProbability <em>Estimated Probability</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.NFRBeliefImpl#isSatisfied <em>Satisfied</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NFRBeliefImpl extends MinimalEObjectImpl.Container implements NFRBelief {
	/**
	 * The cached value of the '{@link #getNfr() <em>Nfr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNfr()
	 * @generated
	 * @ordered
	 */
	protected NFR nfr;

	/**
	 * The default value of the '{@link #getEstimatedProbability() <em>Estimated Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEstimatedProbability()
	 * @generated
	 * @ordered
	 */
	protected static final double ESTIMATED_PROBABILITY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getEstimatedProbability() <em>Estimated Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEstimatedProbability()
	 * @generated
	 * @ordered
	 */
	protected double estimatedProbability = ESTIMATED_PROBABILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SATISFIED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSatisfied() <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSatisfied()
	 * @generated
	 * @ordered
	 */
	protected boolean satisfied = SATISFIED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NFRBeliefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.NFR_BELIEF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NFR getNfr() {
		if (nfr != null && nfr.eIsProxy()) {
			InternalEObject oldNfr = (InternalEObject)nfr;
			nfr = (NFR)eResolveProxy(oldNfr);
			if (nfr != oldNfr) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.NFR_BELIEF__NFR, oldNfr, nfr));
			}
		}
		return nfr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NFR basicGetNfr() {
		return nfr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNfr(NFR newNfr) {
		NFR oldNfr = nfr;
		nfr = newNfr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.NFR_BELIEF__NFR, oldNfr, nfr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getEstimatedProbability() {
		return estimatedProbability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEstimatedProbability(double newEstimatedProbability) {
		double oldEstimatedProbability = estimatedProbability;
		estimatedProbability = newEstimatedProbability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.NFR_BELIEF__ESTIMATED_PROBABILITY, oldEstimatedProbability, estimatedProbability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSatisfied() {
		return satisfied;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSatisfied(boolean newSatisfied) {
		boolean oldSatisfied = satisfied;
		satisfied = newSatisfied;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.NFR_BELIEF__SATISFIED, oldSatisfied, satisfied));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.NFR_BELIEF__NFR:
				if (resolve) return getNfr();
				return basicGetNfr();
			case StormlogPackage.NFR_BELIEF__ESTIMATED_PROBABILITY:
				return getEstimatedProbability();
			case StormlogPackage.NFR_BELIEF__SATISFIED:
				return isSatisfied();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.NFR_BELIEF__NFR:
				setNfr((NFR)newValue);
				return;
			case StormlogPackage.NFR_BELIEF__ESTIMATED_PROBABILITY:
				setEstimatedProbability((Double)newValue);
				return;
			case StormlogPackage.NFR_BELIEF__SATISFIED:
				setSatisfied((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.NFR_BELIEF__NFR:
				setNfr((NFR)null);
				return;
			case StormlogPackage.NFR_BELIEF__ESTIMATED_PROBABILITY:
				setEstimatedProbability(ESTIMATED_PROBABILITY_EDEFAULT);
				return;
			case StormlogPackage.NFR_BELIEF__SATISFIED:
				setSatisfied(SATISFIED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.NFR_BELIEF__NFR:
				return nfr != null;
			case StormlogPackage.NFR_BELIEF__ESTIMATED_PROBABILITY:
				return estimatedProbability != ESTIMATED_PROBABILITY_EDEFAULT;
			case StormlogPackage.NFR_BELIEF__SATISFIED:
				return satisfied != SATISFIED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (estimatedProbability: ");
		result.append(estimatedProbability);
		result.append(", satisfied: ");
		result.append(satisfied);
		result.append(')');
		return result.toString();
	}

} //NFRBeliefImpl
