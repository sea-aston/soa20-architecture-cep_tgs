/**
 */
package uk.ac.aston.stormlog.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import uk.ac.aston.stormlog.RewardTable;
import uk.ac.aston.stormlog.RewardTableRow;
import uk.ac.aston.stormlog.RewardTableThreshold;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reward Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableImpl#getRows <em>Rows</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.RewardTableImpl#getThresholds <em>Thresholds</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RewardTableImpl extends MinimalEObjectImpl.Container implements RewardTable {
	/**
	 * The cached value of the '{@link #getRows() <em>Rows</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRows()
	 * @generated
	 * @ordered
	 */
	protected EList<RewardTableRow> rows;

	/**
	 * The cached value of the '{@link #getThresholds() <em>Thresholds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThresholds()
	 * @generated
	 * @ordered
	 */
	protected EList<RewardTableThreshold> thresholds;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RewardTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.REWARD_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RewardTableRow> getRows() {
		if (rows == null) {
			rows = new EObjectContainmentEList<RewardTableRow>(RewardTableRow.class, this, StormlogPackage.REWARD_TABLE__ROWS);
		}
		return rows;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RewardTableThreshold> getThresholds() {
		if (thresholds == null) {
			thresholds = new EObjectContainmentEList<RewardTableThreshold>(RewardTableThreshold.class, this, StormlogPackage.REWARD_TABLE__THRESHOLDS);
		}
		return thresholds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE__ROWS:
				return ((InternalEList<?>)getRows()).basicRemove(otherEnd, msgs);
			case StormlogPackage.REWARD_TABLE__THRESHOLDS:
				return ((InternalEList<?>)getThresholds()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE__ROWS:
				return getRows();
			case StormlogPackage.REWARD_TABLE__THRESHOLDS:
				return getThresholds();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE__ROWS:
				getRows().clear();
				getRows().addAll((Collection<? extends RewardTableRow>)newValue);
				return;
			case StormlogPackage.REWARD_TABLE__THRESHOLDS:
				getThresholds().clear();
				getThresholds().addAll((Collection<? extends RewardTableThreshold>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE__ROWS:
				getRows().clear();
				return;
			case StormlogPackage.REWARD_TABLE__THRESHOLDS:
				getThresholds().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.REWARD_TABLE__ROWS:
				return rows != null && !rows.isEmpty();
			case StormlogPackage.REWARD_TABLE__THRESHOLDS:
				return thresholds != null && !thresholds.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RewardTableImpl
