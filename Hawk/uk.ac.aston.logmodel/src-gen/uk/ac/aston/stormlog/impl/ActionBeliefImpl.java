/**
 */
package uk.ac.aston.stormlog.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import uk.ac.aston.stormlog.Action;
import uk.ac.aston.stormlog.ActionBelief;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Belief</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.impl.ActionBeliefImpl#getAction <em>Action</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.impl.ActionBeliefImpl#getEstimatedValue <em>Estimated Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionBeliefImpl extends MinimalEObjectImpl.Container implements ActionBelief {
	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * The default value of the '{@link #getEstimatedValue() <em>Estimated Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEstimatedValue()
	 * @generated
	 * @ordered
	 */
	protected static final double ESTIMATED_VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getEstimatedValue() <em>Estimated Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEstimatedValue()
	 * @generated
	 * @ordered
	 */
	protected double estimatedValue = ESTIMATED_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionBeliefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StormlogPackage.Literals.ACTION_BELIEF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction() {
		if (action != null && action.eIsProxy()) {
			InternalEObject oldAction = (InternalEObject)action;
			action = (Action)eResolveProxy(oldAction);
			if (action != oldAction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StormlogPackage.ACTION_BELIEF__ACTION, oldAction, action));
			}
		}
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action basicGetAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.ACTION_BELIEF__ACTION, oldAction, action));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getEstimatedValue() {
		return estimatedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEstimatedValue(double newEstimatedValue) {
		double oldEstimatedValue = estimatedValue;
		estimatedValue = newEstimatedValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StormlogPackage.ACTION_BELIEF__ESTIMATED_VALUE, oldEstimatedValue, estimatedValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StormlogPackage.ACTION_BELIEF__ACTION:
				if (resolve) return getAction();
				return basicGetAction();
			case StormlogPackage.ACTION_BELIEF__ESTIMATED_VALUE:
				return getEstimatedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StormlogPackage.ACTION_BELIEF__ACTION:
				setAction((Action)newValue);
				return;
			case StormlogPackage.ACTION_BELIEF__ESTIMATED_VALUE:
				setEstimatedValue((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StormlogPackage.ACTION_BELIEF__ACTION:
				setAction((Action)null);
				return;
			case StormlogPackage.ACTION_BELIEF__ESTIMATED_VALUE:
				setEstimatedValue(ESTIMATED_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StormlogPackage.ACTION_BELIEF__ACTION:
				return action != null;
			case StormlogPackage.ACTION_BELIEF__ESTIMATED_VALUE:
				return estimatedValue != ESTIMATED_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (estimatedValue: ");
		result.append(estimatedValue);
		result.append(')');
		return result.toString();
	}

} //ActionBeliefImpl
