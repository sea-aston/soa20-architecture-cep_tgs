/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.Observation#getDescription <em>Description</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Observation#getProbability <em>Probability</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.Observation#getMeasurements <em>Measurements</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getObservation()
 * @model
 * @generated
 */
public interface Observation extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getObservation_Description()
	 * @model unique="false"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Observation#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Probability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Probability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Probability</em>' attribute.
	 * @see #setProbability(double)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getObservation_Probability()
	 * @model unique="false"
	 * @generated
	 */
	double getProbability();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.Observation#getProbability <em>Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Probability</em>' attribute.
	 * @see #getProbability()
	 * @generated
	 */
	void setProbability(double value);

	/**
	 * Returns the value of the '<em><b>Measurements</b></em>' containment reference list.
	 * The list contents are of type {@link uk.ac.aston.stormlog.Measurement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measurements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measurements</em>' containment reference list.
	 * @see uk.ac.aston.stormlog.StormlogPackage#getObservation_Measurements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Measurement> getMeasurements();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" measureNameUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%uk.ac.aston.stormlog.Measurement%&gt;&gt; _measurements = this.getMeasurements();\nfor (final &lt;%uk.ac.aston.stormlog.Measurement%&gt; m : _measurements)\n{\n\t&lt;%java.lang.String%&gt; _name = m.getMeasure().getName();\n\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_name, measureName);\n\tif (_equals)\n\t{\n\t\treturn m;\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	Measurement getFirstMeasurement(String measureName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" measureUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='&lt;%org.eclipse.emf.common.util.EList%&gt;&lt;&lt;%uk.ac.aston.stormlog.Measurement%&gt;&gt; _measurements = this.getMeasurements();\nfor (final &lt;%uk.ac.aston.stormlog.Measurement%&gt; m : _measurements)\n{\n\t&lt;%uk.ac.aston.stormlog.Measure%&gt; _measure = m.getMeasure();\n\tboolean _equals = &lt;%com.google.common.base.Objects%&gt;.equal(_measure, measure);\n\tif (_equals)\n\t{\n\t\treturn m;\n\t}\n}\nreturn null;'"
	 * @generated
	 */
	Measurement getFirstMeasurement(Measure measure);

} // Observation
