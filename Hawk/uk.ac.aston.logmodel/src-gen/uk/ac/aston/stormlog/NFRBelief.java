/**
 */
package uk.ac.aston.stormlog;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>NFR Belief</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link uk.ac.aston.stormlog.NFRBelief#getNfr <em>Nfr</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.NFRBelief#getEstimatedProbability <em>Estimated Probability</em>}</li>
 *   <li>{@link uk.ac.aston.stormlog.NFRBelief#isSatisfied <em>Satisfied</em>}</li>
 * </ul>
 *
 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRBelief()
 * @model
 * @generated
 */
public interface NFRBelief extends EObject {
	/**
	 * Returns the value of the '<em><b>Nfr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nfr</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nfr</em>' reference.
	 * @see #setNfr(NFR)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRBelief_Nfr()
	 * @model
	 * @generated
	 */
	NFR getNfr();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.NFRBelief#getNfr <em>Nfr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nfr</em>' reference.
	 * @see #getNfr()
	 * @generated
	 */
	void setNfr(NFR value);

	/**
	 * Returns the value of the '<em><b>Estimated Probability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Estimated Probability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Estimated Probability</em>' attribute.
	 * @see #setEstimatedProbability(double)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRBelief_EstimatedProbability()
	 * @model unique="false"
	 * @generated
	 */
	double getEstimatedProbability();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.NFRBelief#getEstimatedProbability <em>Estimated Probability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Estimated Probability</em>' attribute.
	 * @see #getEstimatedProbability()
	 * @generated
	 */
	void setEstimatedProbability(double value);

	/**
	 * Returns the value of the '<em><b>Satisfied</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfied</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfied</em>' attribute.
	 * @see #setSatisfied(boolean)
	 * @see uk.ac.aston.stormlog.StormlogPackage#getNFRBelief_Satisfied()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSatisfied();

	/**
	 * Sets the value of the '{@link uk.ac.aston.stormlog.NFRBelief#isSatisfied <em>Satisfied</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Satisfied</em>' attribute.
	 * @see #isSatisfied()
	 * @generated
	 */
	void setSatisfied(boolean value);

} // NFRBelief
